﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Pause : MonoBehaviour
{
    private bool status;
    public GameObject panel;
    void Start()
    {
        status = false;
    }

    void Update()
    {
        if (status)
        {
            panel.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            panel.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void PauseGame()
    {
        status = !status;
    }
    public void ResumeGame()
    {
        status = !status;
    }

    public void GoToMapList()
    {
        SceneManager.LoadScene("LevelList");
    }

}
