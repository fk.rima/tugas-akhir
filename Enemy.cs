﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Nodes
{
    public Vector3 position { get; set; }
    public int fcost { get; set; }
    public int gcost { get; set; }
    public int hcost { get; set; }
}

public class Enemy : MonoBehaviour
{
    public float speed;
    GameObject target;
    private Vector3 targetPos;
    private Vector3 startPos;
    private Rigidbody2D enemyBody;
    List<Vector3> path = new List<Vector3>();
    private int o;
    void Start()
    {
        target = GameObject.Find("Player");
        targetPos = target.transform.position;
        startPos = this.transform.position;
        enemyBody = GetComponent<Rigidbody2D>();
        path = Astarsearch(startPos, targetPos);
        path.Reverse();
    }
    void Update()
    {
        if (o < path.Count - 1)
        {
            if (transform.position != path[o + 1])
            {
                Vector3 pos = Vector3.MoveTowards(transform.position, path[o + 1], speed * Time.deltaTime);
                enemyBody.MovePosition(pos);
            }
            else
            {
                o = o + 1;
            }
        }
        else
        {
            path.Clear();
            path = Astarsearch(transform.position, GameObject.Find("Player").transform.position);
            path.Reverse();
            o = 0;
        }
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "Player")
        {
            Lives.livesAmount -= 1;
            SFXManager.PlaySFX("attack");
        }
    }

    List<Vector3> Astarsearch(Vector3 n, Vector3 g)
    {
        //Debug.Log("gx: " + g.x + ",gy:" + g.y);
        g.x = (int)g.x;
        g.y = (int)g.y;
        n.x = (int)n.x;
        n.y = (int)n.y;
        if(CheckIsWall(g))
        {
            g = n;
        }
        //Debug.Log("gx int: " + g.x + ",gy int:" + g.y);
        List<Nodes> Open_nodes = new List<Nodes>();
        List<Nodes> Closed_nodes = new List<Nodes>();
        Nodes best_node = new Nodes();
        int i = 0;
        int l = 0;
        Open_nodes.Insert(i, new Nodes() { position = n, fcost = (int)ManhattanDistance(g, n), gcost = 0});
        i = i + 1;
        while (true)
        {
            if (Open_nodes.Count > 1)
            {
                int index = Open_nodes.Count - 1;
                int mins = index;
                int val = Open_nodes[index].fcost;
                for (int m = Open_nodes.Count - 2; m >= 0; m--)
                {
                    if (val > Open_nodes[m].fcost)
                    {
                        val = Open_nodes[m].fcost;
                        mins = m;
                    }
                }
                best_node = Open_nodes[mins];
                /*for (int k = 1; k < Open_nodes.Count; k++)
                {
                    if (min > Open_nodes[k].fcost)
                    {
                        min = Open_nodes[k].fcost;
                        best_node = Open_nodes[k];
                    }
                }*/
            }
            else
            {
                best_node = Open_nodes[0];
            }

            //Debug.Log("pos: " + best_node.position + ", gcost: " + best_node.gcost);
            Closed_nodes.Insert(l, best_node);
            Open_nodes.Remove(best_node);
            i = i - 1;
            l = l + 1;
            
            if (best_node.position == g)
            {
                break;
            }
            else
            {
                /*if (Mathf.Abs(best_node.position.x - g.x) == 1 || Mathf.Abs(best_node.position.y - g.y) == 1)
                {
                    if (Mathf.Abs(best_node.position.x - g.x) == 0 || Mathf.Abs(best_node.position.y - g.y) == 0)
                    {
                        Debug.Log("check");
                        break;
                    }
                }*/
                Nodes[] successor = new Nodes[4];
                successor[0] = new Nodes() { position = new Vector3(best_node.position.x, best_node.position.y + 1) };
                successor[1] = new Nodes() { position = new Vector3(best_node.position.x + 1, best_node.position.y) };
                successor[2] = new Nodes() { position = new Vector3(best_node.position.x, best_node.position.y - 1) };
                successor[3] = new Nodes() { position = new Vector3(best_node.position.x - 1, best_node.position.y) };

                for (int j = 0; j < 4; j++)
                {
                    if (CheckIsWall(successor[j].position))
                    {
                        successor[j].gcost = 999999;
                    }
                    else
                    {
                        successor[j].gcost = best_node.gcost + 1;
                    }
                    int m = 0;
                    while (true)
                    {
                        if (successor[j].position == Closed_nodes[m].position)
                        {
                            successor[j].gcost = 999999;
                            break;
                        }
                        m = m + 1;
                        if (m >= Closed_nodes.Count)
                        {
                            break;
                        }
                    }
                    
                    int p = 0;
                    while (true)
                    {
                        if (Open_nodes.Any())
                        {
                            if (successor[j].position == Open_nodes[p].position)
                            {
                                if (successor[j].gcost < Open_nodes[p].gcost)
                                {
                                    Open_nodes[p].gcost = successor[j].gcost;
                                    Open_nodes[p].fcost = Open_nodes[p].gcost + Open_nodes[p].hcost;
                                    break;
                                }
                                break;
                            }
                        }

                        p = p + 1;
                        if (p >= Open_nodes.Count)
                        {
                            if (successor[j].gcost != 999999)
                            {
                                Open_nodes.Insert(index: i, successor[j]);
                                Open_nodes[i].hcost = (int)ManhattanDistance(g, Open_nodes[i].position);
                                Open_nodes[i].gcost = successor[j].gcost;
                                Open_nodes[i].fcost = Open_nodes[i].gcost + Open_nodes[i].hcost;
                                //Debug.Log(Open_nodes[i].position);
                                i = i + 1;
                                break;
                            }
                            break;
                        }

                    }
                }
            }

        }
        // back-tracking
        List<Vector3> path = new List<Vector3>();
        int index2 = Closed_nodes.Count - 1;
        int goalGcost = Closed_nodes[index2].gcost;
        int temp = 1;
        path.Insert(0, Closed_nodes[index2].position);
        for (int t = Closed_nodes.Count - 2; t>=0; t--)
        {
            if (Closed_nodes[t].gcost == goalGcost - temp)
            {
                if (Mathf.Abs(Closed_nodes[t].position.x - path[temp - 1].x) == 1 || Mathf.Abs(Closed_nodes[t].position.y - path[temp - 1].y) == 1)
                {
                    if (Mathf.Abs(Closed_nodes[t].position.x - path[temp - 1].x) == 0 || Mathf.Abs(Closed_nodes[t].position.y - path[temp - 1].y) == 0)
                    {
                        path.Insert(temp, Closed_nodes[t].position);
                        //Debug.Log("pos:" + Closed_nodes[t].position + "gcost:" + Closed_nodes[t].gcost);
                        temp = temp + 1;
                    }
                }
            }
        }
        return path;
    }

    float ManhattanDistance(Vector3 target, Vector3 openNode)
    {
        return Mathf.Abs(openNode.x - target.x) + Mathf.Abs(openNode.y - target.y);
    }

    bool CheckIsWall(Vector3 coordinate)
    {
        var wall = GameObject.Find("Wall");
        CompositeCollider2D col;
        col = wall.GetComponent<CompositeCollider2D>();
        return col.OverlapPoint(new Vector2(coordinate.x, coordinate.y));
    }

}
