﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Item : MonoBehaviour
{
    private string[] rumah = {"Rumah Gadang", "Rumah Joglo", "Rumah Krong Bade", "Rumah Tambi", "Rumah Honai", "Rumah Bolon", "Rumah Bubungan Lima", "Rumah Selaso Jatuh Kembar", "Rumah Kaki Seribu", "Rumah Joglo Situbondo"};
    private string[] pakaian = { "Aceh", "Jawa Timur", "Jawa Tengah", "Bali", "Yogyakarta", "DKI Jakarta", "Kalimantan Utara", "Papua Barat", "Sumatera Barat", "Riau" };
    private string[] category = new string[10];
    private static int index, index2;
    private Vector3[] newPos1 = { new Vector3(-6, 12), new Vector3(0.5f, 5), new Vector3(-5, -1), new Vector3(23, -3), new Vector3(10, -3.2f), new Vector3(5.5f, 13),new Vector3(7.27f,9), new Vector3(-0.5f, 8), new Vector3(-2.5f, 2), new Vector3(18.13f, 13.13f) };
    private Vector3[] newPos2 = { new Vector3(-3.5f, 11), new Vector3(11, 8), new Vector3(18, 9.3f), new Vector3(17, -1), new Vector3(-6.5f, 12.7f), new Vector3(-1.5f, -3), new Vector3(23.5f, 9), new Vector3(7, 9.8f), new Vector3(-7, -0.4f), new Vector3(20, -1) };
    private Vector3[] newPos = new Vector3[10];
    private void Start()
    {
        index = 0; // inVal = rumah gadang
        index2 = 0;
        if (SceneManager.GetActiveScene().name == "Map1")
        {
            ItemText.item_name = rumah[index]; // update text
            rumah.CopyTo(category, 0);
            newPos1.CopyTo(newPos, 0);
        }
        else
        {
            ItemText.item_name = pakaian[index]; // update text
            pakaian.CopyTo(category, 0);
            newPos2.CopyTo(newPos, 0);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) // dipanggil ketika item dikoleksi oleh player
    {
        if(collision.name == "Player")
        {
            string objects;
            objects = gameObject.name; // ambil nama objek yang dikoleksi
            if (objects != category[index]) // cek jika nama objek tidak sama dengan tulisan di papan (objek salah)
            {
                //putar sfx
                SFXManager.PlaySFX("false");
                // pindahkan objek di posisi lain
                if (index2 < 10)
                {
                    transform.position = newPos[index2];
                }
                else
                {
                    transform.position = newPos[index2 % 10];
                }
                
                // clone musuh jika jumlah musuh kurang dari 5
                Enemy NPC = GameObject.Find("Enemy").GetComponent<Enemy>();
                float[] speedArr = { 1.8f, 2.5f, 3f };

                if (index2 < 3)
                {
                    Enemy clone = Instantiate(NPC, new Vector3(-7, -3), Quaternion.identity);
                    clone.speed = speedArr[index2];
                }
                index2 = index2 + 1;

            }
            else // jika objek benar
            {
                SFXManager.PlaySFX("true");
                Destroy(gameObject); // menghilangkan objek yang dikoleksi
                index = index + 1; // tambah index untuk mengganti text
            }

            if (index <= 9) // jumlah item
            {
                ItemText.item_name = category[index];
            }
            else
            {
                ItemText.item_name = "Pintu terbuka!";
                Door.isLocked = false; // menghapus objek pintu
            }
        }
    }

}
