﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public static bool isLocked;
    void Start()
    {
        isLocked = true;
    }

    void Update()
    {
        if(isLocked == false)
        {
            Destroy(gameObject);
        }
    }
}
