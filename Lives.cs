﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Lives : MonoBehaviour
{
    public static int livesAmount;
    Text text;
    void Start()
    {
        text = GetComponent<Text>();
        livesAmount = 1;
    }

    void Update()
    {
        text.text = livesAmount.ToString();
        if (livesAmount == 0)
        {
            SFXManager.PlaySFX("attack");
            SceneManager.LoadScene("GameOver");
        }
    }
}
