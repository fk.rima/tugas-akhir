﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraMovement : MonoBehaviour
{
    public Transform mainObject;
    public float fraction;
    public Vector2 max;
    public Vector2 min;
   
    void Start()
    {
        
    }

    void Update()
    {
        //pergerakan kamera mengikuti player dengan fraction
        if(transform.position != mainObject.position)
        {
            Vector3 objPosition = new Vector3(mainObject.position.x, mainObject.position.y, transform.position.z);
            objPosition.x = Mathf.Clamp(objPosition.x, min.x, max.x);
            objPosition.y = Mathf.Clamp(objPosition.y, min.y, max.y);
            transform.position = Vector3.Lerp(transform.position, objPosition, fraction);
        }
    }
}
