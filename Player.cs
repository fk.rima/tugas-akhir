﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed;
    private Rigidbody2D myRigidbody;
    private Vector3 move;
    private Animator animator;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        move = Vector3.zero;
        move.x = Input.GetAxisRaw("Horizontal");
        move.y = Input.GetAxisRaw("Vertical");
        if(move != Vector3.zero)
        {
            MovePlayer();
            animator.SetFloat("moveX", move.x);
            animator.SetFloat("moveY", move.y);
        }
          
        if(transform.position.x > 25.30)
        {
            SceneManager.LoadScene("GameWin");
        }
    }

    void MovePlayer()
    {
        if (move.x == 0 || move.y == 0)
            myRigidbody.MovePosition(transform.position + move * speed * Time.deltaTime);
    }
}
