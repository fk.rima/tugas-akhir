﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesIncrease : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SFXManager.PlaySFX("food");
        Lives.livesAmount += 1;
        Destroy(gameObject);
    }
}
